Install XAMPP
Start Apache and SQL
Navigate to http://localhost/phpmyadmin/
Create a new database called 'musicschool' (important)
Download repo
Save website contet only to desktop folder named "musicschool"
Run "Desktop/musicschool/PowerShellScripts/1.SiteSetup.ps1" powershell script. This will make the migrations and populate the database with all of the correct fields
Navigate to localhost:8000 to access the website.

To import test data, run the "Desktop/musicschool/PowerShellScripts/2.Insert Test Data.ps1" script. This will insert the Admin credentials, Students, Classess, and Teachers

Once the database has been populated with the test data, you can login using the following ID's:

Admin
U: SuperUser
P: P@ssword1

Students: (Student1-Student8 following this convention)
U: Student1
P: Student1

Teachers: (Teacher1-Teacher5 following this convention)
U: Teacher1
P: Teacher1

NOTE: you can manually run the "Desktop/musicschool/script.sql" in the phpmyadmin portal against the musicschoolwebsite.