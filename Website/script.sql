INSERT INTO `accounts_accounts` (`id`, `password`, `last_login`, `is_superuser`, `username`, `is_staff`, `is_active`, `email`, `first_name`, `last_name`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$36000$vrUsNiiNeWob$BSeb9wtcg0pWSTpdBFmG+RzmvMZeOB8cumtWSaAuX6g=', '2018-05-30 02:17:25.954809', 1, 'SuperUser', 1, 1, 'random@rando.com.au', 'Mika', 'Williams', '2018-05-30 02:02:17.662870'),
(2, 'pbkdf2_sha256$36000$1HGRqiDkQFb9$b+elQnO0ngm0N+EJbmessecUKesUFiQGh3/7T5cUU2A=', NULL, 0, 'Teacher1', 0, 1, 'teacher1@gmail.com', 'TeacherFirst1', 'TeacherLast1', '2018-05-30 02:03:55.108086'),
(3, 'pbkdf2_sha256$36000$vLgF65Nov1Zk$gjQoYt9l3sH/4ljWPJdeBrkPKUCInuRT6hVJWpThN2A=', NULL, 0, 'Teacher2', 0, 1, 'teacher2@gmail.com', 'TeacherFirst2', 'TeacherLast2', '2018-05-30 02:04:42.397396'),
(4, 'pbkdf2_sha256$36000$UrFugry80QHn$AVF7TmGA07QwbodGpbnJq37iN2715L8Wfp/Ws2xCaqk=', NULL, 0, 'Teacher3', 0, 1, 'teacher3@gmail.com', 'TeacherFirst3', 'TeacherLast3', '2018-05-30 02:05:35.398174'),
(5, 'pbkdf2_sha256$36000$5G5zwi8UtCzF$YzZBpPo/QZEDqpU+iN8AStolvXETGnuoF4rnXhRGJ04=', NULL, 0, 'Teacher4', 0, 1, 'teacher4@gmail.com', 'TeacherFirst4', 'TeacherLast4', '2018-05-30 02:05:59.569916'),
(6, 'pbkdf2_sha256$36000$0tHEC38wQnfh$M/yUhFVWOZpTsz5yJzHgqgvaIhATa5EAdbJEq6yPaIo=', NULL, 0, 'Teacher5', 0, 1, 'teacher5@gmail.com', 'TeacherFirst5', 'TeacherLast5', '2018-05-30 02:06:40.810732'),
(7, 'pbkdf2_sha256$36000$znT0aAEKwIK9$2ojWeTKlsSuuX2L5n0ISG/VxvL3bhZKwbBGVAXGui3A=', NULL, 0, 'Student1', 0, 1, 'student1@gmail.com', 'StudentFirst1', 'StudentLast1', '2018-05-30 02:09:54.325049'),
(8, 'pbkdf2_sha256$36000$wedNruWsVHto$plth0CshRXP13/crBlPWnuS8LmJlfSmnGyvi0nSSRe0=', NULL, 0, 'Student2', 0, 1, 'student2@gmail.com', 'StudentFirst2', 'StudentLast2', '2018-05-30 02:10:48.097977'),
(9, 'pbkdf2_sha256$36000$JrjeCOJpqvFq$ZKnUHQI15Hl3GYSOnuuT4x27E0QSJmqPoA/WfLmra6A=', NULL, 0, 'Student3', 0, 1, 'student3@gmail.com', 'StudentFirst3', 'StudentLast3', '2018-05-30 02:11:25.631167'),
(10, 'pbkdf2_sha256$36000$aEUlhvEyqIsx$MC5YMU9ah/5vEYG6aQVWUeuhyZUSbHvlcegZq4ePwj0=', NULL, 0, 'Student4', 0, 1, 'student4@gmail.com', 'StudentFirst4', 'StudentLast4', '2018-05-30 02:12:16.873047'),
(11, 'pbkdf2_sha256$36000$HMc9utyhw2gU$KXw1FZLMNwaKBiapmlrAnYb6iszADcBPUT5wX0CU2CA=', NULL, 0, 'Student5', 0, 1, 'student5@gmail.com', 'StudentFirst5', 'StudentLast5', '2018-05-30 02:12:57.286345'),
(12, 'pbkdf2_sha256$36000$tcJ2jRx6kv9b$ZrguNOaKxxbXUGnftJzYm8yigHhPa+mLkzcr8zWCnPI=', NULL, 0, 'Student6', 0, 1, 'student6@gmail.com', 'StudentFirst6', 'StudentLast6', '2018-05-30 02:14:57.388154'),
(13, 'pbkdf2_sha256$36000$R8I1GqvUffcB$nzbSodgSQ3XJUVHxpEJgJCHrkSqFFuCG+xbOnwwyQfI=', NULL, 0, 'Student7', 0, 1, 'student7@gmail.com', 'StudentFirst7', 'StudentLast7', '2018-05-30 02:15:43.054546'),
(14, 'pbkdf2_sha256$36000$T0rMb0cKXidx$xPijv6CKmcGGpD7q7LL4iaJT5dUVti/olw0lCaHxpXg=', NULL, 0, 'Student8', 0, 1, 'student8@gmail.com', 'StudentFirst8', 'StudentLast8', '2018-05-30 02:16:49.601609');


INSERT INTO `accounts_teacherdetails` (`id`, `first_name`, `last_name`, `is_teacher`, `birth_date`, `user_id`) VALUES
(1, 'TeacherFirst1', 'TeacherLast1', 1, NULL, 2),
(2, 'TeacherFirst2', 'TeacherLast2', 1, NULL, 3),
(3, 'TeacherFirst3', 'TeacherLast3', 1, NULL, 4),
(4, 'TeacherFirst4', 'TeacherLast4', 1, NULL, 5),
(5, 'TeacherFirst5', 'TeacherLast5', 1, NULL, 6);


INSERT INTO `accounts_musicclass` (`id`, `day`, `class_name`, `instrument_taught`, `class_level`, `class_time`, `birth_date`, `class_room`, `teacher_id`) VALUES
(1, 1, 'P101', 'Piano', 'Beginner', 11, NULL, 'Auditorium', 1),
(2, 4, 'P201', 'Piano', 'Intermediate', 9, NULL, 'Back Room', 2),
(3, 0, 'P301', 'Piano', 'Expert', 17, NULL, 'Main Hall', 3),
(4, 2, 'G101', 'Guitar', 'Beginner', 10, NULL, 'Auditorium', 4),
(5, 3, 'G101', 'Guitar', 'Beginner', 10, NULL, 'Auditorium', 5),
(6, 5, 'G201', 'Guitar', 'Intermediate', 14, NULL, 'Auditorium', 5),
(7, 5, 'V101', 'Violin', 'Beginner', 15, NULL, 'Auditorium', 1),
(8, 0, 'V301', 'Violin', 'Expert', 11, NULL, 'Main Hall', 1),
(9, 2, 'F101', 'Flute', 'Introductory', 16, NULL, 'Auditorium', 3);




INSERT INTO `accounts_studentprofile` (`id`, `first_name`, `last_name`, `AMEB_Ratings`, `birth_date`, `is_student`, `adult_contact_name`, `adult_phone`, `student_class_id`, `user_id`) VALUES
(1, 'StudentFirst1', 'StudentLast1', 1, '2005-03-31', 1, 'John Smith', '123456789', NULL, 7),
(2, 'StudentFirst2', 'StudentLast2', 2, '2001-07-15', 1, 'John Smith', '123456789', NULL, 8),
(3, 'StudentFirst3', 'StudentLast3', 3, '1998-09-22', 1, '', '', NULL, 9),
(4, 'StudentFirst4', 'StudentLast4', 4, '1995-10-01', 1, '', '', NULL, 10),
(5, 'StudentFirst5', 'StudentLast5', 5, '1990-05-19', 1, '', '', NULL, 11),
(6, 'StudentFirst6', 'StudentLast6', 6, '2007-12-11', 1, 'John Smith', '123456789', NULL, 12),
(7, 'StudentFirst7', 'StudentLast7', 7, '2002-04-15', 1, 'John Smith', '123456789', NULL, 13),
(8, 'StudentFirst8', 'StudentLast8', 8, '2001-02-12', 1, 'John Smith', '0712345678', NULL, 14);

