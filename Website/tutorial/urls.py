"""tutorial URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from accounts import views
 
# imports the login 'view' from django we can use
from django.contrib.auth.views import login, logout

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^home/$', views.home, name='home'),

    url(r'^$', views.login_redirect, name='login_redirect'),
    # Since we don't have access to login 'view' - we directly connect this view to our template 'accounts/login.html' (via dictionary {})
    url(r'^login/$', login, {'template_name': 'accounts/login.html'} ),
    url(r'^logout/$', logout, {'template_name': 'accounts/logout.html'} ),

    url(r'^enrol/$', views.studentEnrol, name="studentEnrol"),

    url(r'^register/$', views.registerStudent, name="registerStudent" ),
    url(r'^register/teacher$', views.teacherApplication, name="teacherApplication" ),
    url(r'^profile/$', views.view_profile, name="view_profile" ),
    url(r'^profile/edit$', views.edit_profile, name="edit_profile" ),
    url(r'^timetable/', views.timetable,name = "timetable"),
    url(r'^myclasses', views.myClasses, name = 'myClasses'),
    url(r'^about/', views.about,name="about"),
    url(r'^createteacher/$',views.createteacher,name="createteacher"),
        
   
]
