from django import forms
from django.contrib.auth import get_user_model
from datetime import date
from .models import *
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.forms import UserCreationForm

#Note on defining fields
#As we are using model fields, we can use Django to make fields for us by defining them in the meta class
#However, if we want to make changes to these fields, we must define our own. 


class EditProfileForm(UserChangeForm):
        class Meta:
                model = get_user_model()
                # Create fields variable get has all the fields we want to show 
                fields = (
                        'email',
                        'first_name',
                        'last_name',
                        'password'
                )


class StudentResistrationForm(forms.ModelForm):
        class Meta:
            model = StudentProfile
            fields = (  
                        'first_name',
                        'last_name',
                        'AMEB_Ratings'
                        )
        birth_date=forms.DateField(label="Date of birth (D/M/Y)",input_formats=['%d/%m/%Y'])
        contact_name=forms.CharField(label='Adult contact name',required=False)
        phone_number=forms.RegexField(label='Adult contact phone number', required = False,
                                      regex="\d{8,10}",
                                      error_messages={
                                              'invalid':("Please enter an proper phone number")
                                              }
                                      )



        #Save the field data
        def save(self, commit=True):
                user = super(StudentResistrationForm, self).save(commit=False)
                user.AMEB_Ratings = self.cleaned_data['AMEB_Ratings']
                user.birth_date=self.cleaned_data['birth_date']
                user.adult_contact_name=self.cleaned_data['contact_name']
                user.adult_phone=self.cleaned_data['phone_number']
                user.is_student=True;


                if commit:
                        user.save()

                return user
        #The following are overwrite methods for validating user input on the form
        #In this case, basically makes sure that age requirements are met
        def clean_birth_date(self):
                dob = self.cleaned_data['birth_date']
                age = (date.today()-dob).days/365
                if age < 10:
                        raise forms.ValidationError('Students mst be atleast 10 years old to enrol')
                return dob
        def clean_contact_name(self):
                if self.cleaned_data.get('birth_date'):
                        name = self.cleaned_data['contact_name']
                        dob = self.cleaned_data.get('birth_date')
                        age = (date.today()-dob).days/365
                        if not name and age<18:
                                raise forms.ValidationError('Minors must provide adult contact name')
                        return self.cleaned_data['contact_name']
        def clean_phone_number(self):
                if self.cleaned_data.get('birth_date'):
                        number=self.cleaned_data['phone_number']
                        dob = self.cleaned_data.get('birth_date')
                        age = (date.today()-dob).days/365
                        if not number and age<18:
                                raise forms.ValidationError('Minors must provide adult contact phone number')
                        return self.cleaned_data['phone_number']

class TeacherRegistrationForm(forms.ModelForm):
        class Meta:
            model = TeacherApplications
            fields = (
                'first_name',
                'last_name',
                'instrument',
                'skill',
                'email',
                'experience_in_years',
            )

        phone_number=forms.RegexField(label='Best contact number', required = False,
                                      regex="\d{8,10}",
                                      error_messages={
                                              'invalid':("Please enter an proper phone number")
                                              }
                                      )
            
        def save(self, commit=True):
                user = super(TeacherRegistrationForm, self).save(commit=False)
                user.is_teacher=True
                user.birth_date=date.today()
                user.phone=self.cleaned_data['phone_number']
                if commit:
                        user.save()

                return user
        def clean_first_name(self):
                data = self.cleaned_data['first_name']
                if not data:
                        raise forms.ValidationError('Please provide your first name')
                return data
        def clean_last_name(self):
                data = self.cleaned_data['last_name']
                if not data:
                        raise forms.ValidationError('Please provide your last name')
                return data
        def clean_instrument(self):
                data = self.cleaned_data['instrument']
                if not data:
                        raise forms.ValidationError('Please provide your main instrument')
                return data
        def clean_skill(self):
                data = self.cleaned_data['skill']
                if not data:
                        raise forms.ValidationError('Please tell us your skill level or ameb ranking with your instrument')
                return data
        def clean_experience_in_years(self):
                data = self.cleaned_data['experience_in_years']
                if not data:
                        raise forms.ValidationError('Please provide your teaching experience')
                return data
        def clean_phone_number(self):
                data = self.cleaned_data['phone_number']
                if not data:
                        raise forms.ValidationError('Please provide your best contact number')
                return data
        
class TeacherCreationForm(forms.ModelForm):
        class Meta:
                model=TeacherDetails
                fields=('first_name',
                        'last_name',)
        def clean_first_name(self):
                data = self.cleaned_data['first_name']
                if not data:
                        raise forms.ValidationError('Please enter the teachers first name')
                return data
        def clean_last_name(self):
                data = self.cleaned_data['last_name']
                if not data:
                        raise forms.ValidationError('Please enter the teachers last name')
                return data                
                        
   
class UserForm(forms.ModelForm):
        class Meta:
                model = get_user_model()
                fields = ('username', 'email',)
        password = forms.CharField(widget=forms.PasswordInput())

        

class MusicClassesEnrolForm(forms.ModelForm):
    class Meta:
        model = MusicClass
        fields = ('class_name', 'class_level', 'class_time', 'class_room', 'instrument_taught', 'day', 'teacher')
