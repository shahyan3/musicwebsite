from django.shortcuts import render
from django.template import loader
from django.shortcuts import redirect
from accounts.forms import StudentResistrationForm, TeacherRegistrationForm, EditProfileForm, UserForm, TeacherRegistrationForm, MusicClassesEnrolForm,TeacherCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from accounts.models import Accounts, StudentProfile, TeacherApplications, MusicClass
from django.contrib.auth.forms import UserChangeForm
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required

def home(request):
	return render(request, 'accounts/home.html')

def login_redirect(request):
	return redirect('/login/')

@login_required	
def view_profile(request):
 	args = {'user': request.user}
 	return render(request, 'accounts/profile.html', args)
 	
@login_required	
def edit_profile(request):
 	# Handle post request - if the user submits a form change form details and pass the intance user
 	user = request.user
 	if request.method == 'POST':
 		form = EditProfileForm(request.POST, instance=request.user)
 		if form.is_valid():
                        if hasattr(user,'teacher_profile'):
                                user.teacher_profile.first_name=form.cleaned_data['first_name']
                                user.teacher_profile.last_name=form.cleaned_data['last_name']
                                user.teacher_profile.save()
                        if hasattr(user,'student_profile'):
                                user.student_profile.first_name=form.cleaned_data['first_name']
                                user.student_profile.last_name=form.cleaned_data['last_name']
                                user.student_profile.save()
                        form.save()
                        return redirect('../profile')
  	# Handles the get request - if no post info is submitted then get the form and display it on the edit profile page. 
 	else:
 		form = EditProfileForm(instance=request.user)
 	args = {'form': form}
 	return render(request, 'accounts/profile_edit.html', args)

def registerStudent(request):
    # Once register page loads, either it will send to the server POST data (if the form is submitted), else if it don't send post data create a user form to register
    if request.method == "POST":
        user_form = UserForm(request.POST)
        form = StudentResistrationForm(request.POST)

        if form.is_valid() and user_form.is_valid():
            User = get_user_model()
            username = user_form.cleaned_data['username']
            email = user_form.cleaned_data['email']
            password = user_form.cleaned_data['password']
            fname=form.cleaned_data['first_name']
            lname=form.cleaned_data['last_name']
            new_user = User.objects.create_user(username=username, email=email, password=password,first_name=fname,last_name=lname)
            

            student = form.save(commit=False)
            student.user = new_user
            student.save()

            new_user.save()

            return redirect('/')
    else:
        #  Create the django default user form and send it as a dictionary in args to the reg_form.html page.
        user_form = UserForm()
        form = StudentResistrationForm()        

        # args = {'form_student': form, 'user_form': user_form }
    return render(request, 'accounts/reg_form_students.html', {'form_student': form, 'user_form': user_form })

def teacherApplication(request):
    # Once register page loads, either it will send to the server POST data (if the form is submitted), else if it don't send post data create a user form to register
    if request.method == "POST":

        form = TeacherRegistrationForm(request.POST)

        if form.is_valid():
            teacher = form.save(commit=False)
            teacher.save()

            return redirect('/')
    else:
        #  Create the django default user form and send it as a dictionary in args to the reg_form.html page.
        form = TeacherRegistrationForm()  
    return render(request, 'accounts/reg_form_teachers.html', {'form_student': form})
	
def about(request):
        return render(request, 'accounts/about.html')

def timetable(request):
#define variables to make the timetable, pass them to the form
        dayList=[0,1,2,3,4,5,6]
        nameList=['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
        classList=MusicClass.objects.order_by('id')
        template = loader.get_template('accounts/timetable.html')
        timeList=[9,10,11,12,13,14,15,16,17]
                        
        context = {'dayList':dayList,
                'timeList':timeList,
                'classList':classList,
                'nameList':nameList
                   }
        return HttpResponse(template.render(context,request))


def myClasses(request):
#Figure out if the user is a teacher or student or some other possibility.
#Define the variables needed to draw a personalised timetable
#If they are not logged in as a teacher or a student OR their account has been disabled
#handle it appropriately
        user = request.user
        if hasattr(user,'teacher_profile'):
                teacherStatus = request.user.teacher_profile.is_teacher
                if teacherStatus == 1:
                        ID = request.user.teacher_profile.id
                        dayList=[0,1,2,3,4,5,6]
                        nameList=['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
                        classList=MusicClass.objects.order_by('id')
                        template = loader.get_template('accounts/myClassesTeacher.html')
                        timeList=[9,10,11,12,13,14,15,16,17]
                        
                        context = {'dayList':dayList,
                                'timeList':timeList,
                                'classList':classList,
                                'nameList':nameList,
                                'ID':ID}
                        return HttpResponse(template.render(context,request))
                else:
                        return HttpResponse("Your teacher account has been deactivated. Please speak to the school admin staff if you beleive this to be in error");
                
        elif hasattr(user,'student_profile'):
                studentStatus = request.user.student_profile.is_student
                if studentStatus == 1:
                        ID = request.user.student_profile.student_class_id
                        dayList=[0,1,2,3,4,5,6]
                        nameList=['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
                        classList=MusicClass.objects.order_by('id')
                        template = loader.get_template('accounts/myClassesStudent.html')
                        timeList=[9,10,11,12,13,14,15,16,17]                       
                        context = {'dayList':dayList,
                                'timeList':timeList,
                                'classList':classList,
                                'nameList':nameList,
                                'ID':ID}
                        return HttpResponse(template.render(context,request))
                else:
                        return HttpResponse("Your student account has been deactivated. Please speak to the school admin staff if you beleive this to be in error");



        else:
                return HttpResponse("You must be logged in as a teacher or a student to use this feature. Hit back on your browser");

        

@login_required 
def studentEnrol(request):
    user = request.user
    if not hasattr(user,'student_profile'): 
         # The current user doesn't have student_profile attribute
         # redirect to the page you want
         return redirect('/home') # for instance
    student_profile = request.user.student_profile
    if request.method == "POST":
         id_musicClass     = request.POST.get('musicClass')
         music_class = MusicClass.objects.get(id=id_musicClass)
         # or music_class = get_object_or_404(MusicClass,id=id_musicClass)
         student_profile.student_class = music_class
         student_profile.save()
         return redirect('/myclasses') 
    else:
        querySet = MusicClass.objects.all()
        args = {'classes': querySet }
 
    return render(request, 'accounts/enrol.html', args)

def createteacher(request):
    # Once register page loads, either it will send to the server POST data (if the form is submitted), else if it don't send post data create a user form to register
    if request.method == "POST":
        user_form = UserForm(request.POST)
        form = TeacherCreationForm(request.POST)
        if form.is_valid() and user_form.is_valid():
            User = get_user_model()
            username = user_form.cleaned_data['username']
            email = user_form.cleaned_data['email']
            password = user_form.cleaned_data['password']
            fname=form.cleaned_data['first_name']
            lname=form.cleaned_data['last_name']
            new_user = User.objects.create_user(username=username, email=email, password=password,first_name=fname,last_name=lname)

            teacher = form.save(commit=False)
            teacher.user = new_user
            teacher.save()

            new_user.save()

            return redirect('/')
    else:
        #  Create the django default user form and send it as a dictionary in args to the reg_form.html page.
        user_form = UserForm()
        form = TeacherCreationForm()  
    return render(request, 'accounts/createteacher.html', {'form_student': form, 'user_form': user_form })
        
        
    

 
