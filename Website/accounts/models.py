from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.db.models.signals import *
from django.conf import settings

#Use class Meta: to define human readable names for the database for use on admin page etc
#use __str__ method to define human readable names for individual objects in the database

class Accounts(AbstractUser):
    def __str__(self):
       return self.first_name + " " + self.last_name

    email = models.EmailField('email address', unique=True)
    first_name = models.CharField('first name', max_length=30, blank=True)
    last_name = models.CharField('last name', max_length=30, blank=True)
    date_joined = models.DateTimeField('date joined', auto_now_add=True)


class TeacherApplications(models.Model):
    class Meta:
        verbose_name="New Teacher Applications"
        verbose_name_plural="New Teacher Applications"

    def __str__(self):
        return self.first_name + " " + self.last_name

    first_name = models.CharField(max_length=250, blank=True)
    last_name = models.CharField(max_length=250, blank=True)
    instrument = models.CharField(max_length=500, blank=True)
    skill = models.CharField(max_length=30, blank=True)
    email = models.CharField(max_length=30, blank=True)
    experience_in_years = models.PositiveIntegerField(blank=True)
    birth_date = models.DateField(null=True, blank=True)    
    is_teacher = models.BooleanField('teacher status', default=True)
    phone=models.CharField(max_length=250,blank=True)


class MusicClass(models.Model):
    class Meta:
        verbose_name="Music Class"
        verbose_name_plural="Music Classes"

    def __str__(self):
       return self.class_name    
    CLASS_CHOICES=((9,9),(10,10),(11,11),(12,12),(13,13),(14,14),(15,15),(16,16),(17,17))
    DAY_CHOICES=((0,'Sunday'),(1,'Monday'), (2,'Tuesday'),(3,'Wednesday'),(4,'Thursday'),(5,'Friday'),(6,'Saturday'),(7,'Default'))
    teacher = models.ForeignKey('TeacherDetails', related_name='music_class_teacher', null=True)
    day=models.IntegerField(choices=DAY_CHOICES,default=7)
    class_name = models.CharField(max_length=500, blank=True)
    instrument_taught = models.CharField(max_length=500, blank=True)
    class_level = models.CharField(max_length=30, blank=True)
    class_time=models.IntegerField(choices=CLASS_CHOICES,default=9)
    birth_date = models.DateField(null=True, blank=True)
    class_room = models.CharField(max_length=100, blank=False, default = 'Auditorium')
    
class TeacherDetails(models.Model):
    class Meta:
        verbose_name="Hired Teacher Details"
        verbose_name_plural="Hired Teacher Details"

    def __str__(self):
       return self.first_name + " " + self.last_name
    
    user = models.OneToOneField('Accounts', related_name='teacher_profile')
    first_name = models.CharField(max_length=250, blank=True)
    last_name = models.CharField(max_length=250, blank=True)
    is_teacher = models.BooleanField('teacher status', default=True)
    birth_date = models.DateField(null=True, blank=True) 
    

class StudentProfile(models.Model):
    class Meta:
        verbose_name="Student Profile"
        verbose_name_plural="Student Profiles"

    def __str__(self):
       return self.first_name + " " + self.last_name

    user = models.OneToOneField('Accounts', related_name='student_profile')
    first_name = models.CharField(max_length=250, blank=True)
    last_name = models.CharField(max_length=250, blank=True)
    AMEB_Ratings = models.PositiveIntegerField(default=0)
    birth_date = models.DateField(null=True, blank=True)
    student_class = models.ForeignKey(to=MusicClass, related_name="student_class", null=True, blank=True)
    is_student = models.BooleanField('student status', default=True)
    adult_contact_name=models.CharField(max_length=250,blank=True)
    adult_phone=models.CharField(max_length=250,blank=True)




