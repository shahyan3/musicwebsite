from django.contrib import admin

from accounts.models import *

#  Customize admin header - doesn't work now since we have a base.html for admin custom
admin.site.site_header = 'Admin Dashboard'

# This is my admin class which is specifically related to the user profile model (inherits model.admin (admin database table))
class UserProfileAdmin(admin.ModelAdmin): 
	def user_info(self, obj):
		return obj.description

	# Quering the rows based on something - queryset is a method that admin model inherites - we overiding it. Order by status, if can't then order by User -status would order by descending
	def get_queryset(self, request):
		queryset = super(UserProfileAdmin, self).get_queryset(request)
		updatedQuery = queryset.order_by('birth_date') 
		return updatedQuery

	#  Display short name for user info field 'info'
	user_info.short_description = 'Info'

# Register your models here 

admin.site.register(StudentProfile, UserProfileAdmin)

admin.site.register(TeacherApplications, UserProfileAdmin)
	
admin.site.register(MusicClass, UserProfileAdmin)

admin.site.register(TeacherDetails, UserProfileAdmin)

