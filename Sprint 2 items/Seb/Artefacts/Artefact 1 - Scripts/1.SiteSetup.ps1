# navigate to the current user's desktop and the recommended website install directory
cd C:\Users\$env:UserName\Desktop\musicschool

# Make migrations to the /musicschool database
python manage.py showmigrations
python manage.py makemigrations
python manage.py migrate

#Start the server
Write-Host "Starting the Server"
python manage.py runserver